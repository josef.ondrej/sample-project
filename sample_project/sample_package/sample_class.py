from typing import List, Dict, Optional


class SampleClass:
    def __init__(self, sample_argument_one: List[str], sample_argument_two: Dict[str, float],
                 sample_argument_three: Optional[int] = None):
        self._sample_argument_one = sample_argument_one
        self._sample_argument_two = sample_argument_two
        self._sample_argument_three = sample_argument_three

    def __str__(self) -> str:
        string_representation = "Argument one: " + str(self.sample_argument_one) + "\n"
        string_representation += "Argument two: " + str(self.sample_argument_two) + "\n"
        string_representation += "Argument three: " + str(self.sample_argument_three) + "\n"
        return string_representation

    @property
    def sample_argument_one(self) -> List[str]:
        return self._sample_argument_one

    @property
    def sample_argument_two(self) -> Dict[str, float]:
        return self._sample_argument_two

    @property
    def sample_argument_three(self) -> Optional[int]:
        return self._sample_argument_three

    def get_first_item_of_arg_one(self) -> str:
        first_item = self.sample_argument_one[0]
        return first_item


if __name__ == "__main__":
    print("This won't be printed if you import the sample_class.SampleClass in some different file.")
