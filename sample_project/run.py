from sample_project.sample_package.sample_class import SampleClass


def create_sample_class() -> SampleClass:
    sample_argument_one = ["a", "b", "c"]
    sample_argument_two = {"k": 15.0, "l": 16.78, "m": 47.2}
    sample_argument_three = 15

    sample_class = SampleClass(sample_argument_one, sample_argument_two, sample_argument_three)
    return sample_class


if __name__ == "__main__":
    sample_class = create_sample_class()
    print("Hello World!")
    print("=" * 50)
    print(str(sample_class))
