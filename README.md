Sample Project
==============
- This is a sample python project. This is the `README.md` file that should normally contain some description of the 
 project and how to run it, e.g. like the following: 
 
 To run this project just go to the directory `sample-project` and run 
```
python -m pip install -r requirements.txt
python sample_project/run.py
```

### Structuring the Project
- It is important to create the main project directory `sample-project` and inside that put the main package `sample_package`. 
If the names consist of multiple words then use `-` to separate the project directory names and `_` to separate the package name. 
Both names should be in <b>lowercase</b>. 
- It is a good practice to put the `README.md`, `requirements.txt` and `.gitignore` files in the main package as a minimum. 
The purpose of `README.md` is hopefully clear now. The `requirements.txt` contains the required packages that have to be 
installed in python in order to run the project. The `.gitignore` file is a list of files that you don't want to include 
into `git`. These could be for example some temporary files or directories like `__pycache__`. 
There are already some ready-to-use `.gitignore` files (like the one used here) containing a typical selection of such unwanted files.
- Files with similar functionality should be grouped into packages. For example if you 
have a lot of utilities that contain functions for plotting images, you could move them in their 
own package `sample_package/plotting_utils`. Each package (as well as the main package `sample_package`) needs to have the
`__init__.py` file inside it which is typically empty.  
  

### Structuring the Code
- The first point is tightly connected with the first point in the <b>Structuring the Project</b> section. 
If you want to import some module when working in your project you do it using an <b>absolute import</b>: 
    ```python
    from sample_project.sample_package.sample_class import SampleClass
    ``` 
  So you always start the import specification from the main package (`sample_project`) directory. This is very important 
  to avoid name collision in imports in the future, which could become a hell to resolve if you do not structure your project like this 
  from the beginning.
 - All imports should be declared at the top of the file.
 - Always name files, packages, directories in lowercase and use underscore `_` as a separator in case there are more words - we 
 will call this <b>snake_case</b> from now on. 
 - Always name functions using snake case. Use one underscore before the name of private functions (those that you don't want to use 
 outside of the file they are specified in). So for example
    ```python
    def public_function():
        return 1234
           
    def _private_function():
        return 1234
    ```
- Always name classes using so called <b>CamelCase</b> - i.e. start each word in the name with a capital letter and do not use spaces 
between words. For example: 
    ```python
    class SampleClass:
        def __init__(self):
            ...
      
        def public_function(self):
            ...
      
        def _private_function(self):
            ...
    ```
    It is a good practice to have one class per file. So for example class `MyClass` will by in file called `my_class.py`. 
- Use snake_case for variable names. Try to use <b>meaningful names for every variable</b>, 
so it is immediately clear what the variable contains from it's name without additional information about it's context. 
So for example it is better to use `row_index` instead of `i, j, k` when iterating over some array. 
Use `database_file_path` for the path to for example some sqlite file - not just `database`. 
Use `model_with_gaussian_response_and_random_effects` rather than `m`, etc.  
- Use <b>4 spaces</b> (not Tabs) to indent blocks of code. 
- Use typing annotations in function declarations. The basic types you will use are `str`, `float`, `int`, `List`, `Dict`, `Tuple` 
and then of course types of some custom classes that you use. The type can be specified for both 
the arguments and also output of the function (if it has any). So for example:
    ```python
    from typing import List, Dict
    from sample_package.custom_classes.my_class import MyClass
    
    def function_with_lots_of_arguments(arg_one: List[str], arg_two: Dict[str, int], arg_three: MyClass) -> Tuple[str, int, float]:           
        ...
    ```
    The typing annotations greatly improve the readability of the code.   
- Use `if __name__ == "__main__":` block for code that you don't want to run when importing the file.

### General remarks

- Well written code should be self documenting. When you read it, it should immediately be clear from the names and 
typing annotations what is the purpose of each function and the meaning of each variable.  
- When writing the code always try to decompose more complex classes / functions into simpler ones. 
For example if you know that you would need to do several non-trivial operations inside one function, 
then it is a good practice to first create the operations as separate functions (just define them and do not implement them) 
then implement the main function using these utility functions and then implement those utility functions as last. 
For example:
 
  <b>Step 1:</b> Think about how the main function should look like when you would have the utility functions implemented. 
  Do not implement the utility functions just yet - this would only distract you from the bigger picture.   
    ```python
    def main_function(x): 
        if _does_x_belong_to_group_A(x): 
            result = _main_function_group_A(x)
        else: 
            result = _main_function_other_groups(x)
                
        return result
      
    def _does_x_belong_to_group_A(x):
        raise NotImplementedError("TODO: Implement.")
      
    def _main_function_group_A(x):
        raise NotImplementedError("TODO: Implement.")
      
    def _main_function_other_groups(x):
        raise NotImplementedError("TODO: Implement.") 
    ``` 
  <b>Step 2:</b> When you have the main function, then you can start implementing the utility functions one by one. 
    ```python
    def _does_x_belong_to_group_A(x):
        if x in [1,4,8,16] and x ** 2 // 3 < 10: 
            ...
    ```
  
  

